class RPNCalculator
  def initialize
    @stack = []
  end

  def empty?
    if @stack.length < 2
      puts @stack
      raise "calculator is empty"
    end
  end

  def push(value)
    @stack.push(value)
  end

  def plus
    empty?
    fin_value = @stack.pop + @stack.pop
    @stack.push(fin_value)
  end

  def value
    @stack.last
  end

  def minus
    empty?
    fin_value = -@stack.pop + @stack.pop
    @stack.push(fin_value)
  end

  def divide
    empty?
    divisor = @stack.pop.to_f
    denominator = @stack.pop.to_f
    fin_value = denominator / divisor
    @stack.push(fin_value)
  end

  def times
    empty?
    fin_value = @stack.pop * @stack.pop
    @stack.push(fin_value)
  end

  # private
  # def perform_operation(op_symbol)
  #
  #
  #   case op_symbol
  #   when :+
  #   when :-
  #   when :*
  #   when :/
  #     divisor=@stack.pop
  #     denominator=@stack.pop
  #     @stack.push(divisor / denominator)
  #   else
  #     raise "no such operand. #{op_symbol} "
  # end


end
